from django.db import models
from django.contrib.auth.models import AbstractUser, User

# install by pip install django-fernet-fields


# Create your models here.
class MyUser(AbstractUser):
    """Description: Model Description."""

    function = models.CharField(max_length=150, default='shop')

    class Meta:
        """Description: Meta."""

        db_table = 'auth_user'
        unique_together = ('email',)
        User._meta.get_field('username')._unique = False
