# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.contrib.auth import(
    authenticate, login as auth_login,
    logout as auth_logout
)
from django.db.models import Q

from django.shortcuts import redirect
from .models import MyUser

import json
from django.http import JsonResponse


def create_response_user(users_query):
    users = []
    for user in users_query:
        users.append({
            'id': user.id,
            'first_name': user.first_name,
            'last_name': user.last_name,
            'email': user.email,
            'permission': user.function,
        })
    return users


def login(request):
    message = ''
    user = []
    if request.method == 'POST':
        email = request.POST.get('login')
        password = request.POST.get('password')
        try:
            user = MyUser.objects.get(email=email)
        except:
            pass
        if user != []:
            user = authenticate(username=user.username, password=password)
            if user is not None:
                if user.is_active:
                    auth_login(request, user)
                    return redirect('/')
        message = "Email de Usuario o Contraseña no válido"
        return render(request, 'intra/login.html', {'message': message, 'success': True})
    return render(request, 'login.html', {'message': message, 'success': False})


def logout(request):
    """Sistem logout."""
    auth_logout(request)
    return redirect('/')


@login_required(login_url='login/')
def index(request):
    return render(request, 'users.html')


def all_users(request):
    response = {}
    response['users'] = []
    users = MyUser.objects.all().order_by('id')
    response['users'] = create_response_user(users)
    return JsonResponse(response)


def add_user(request):
    message = 'Ok'
    data = request.POST.get('model')
    data = json.loads(data)

    email = data['email']
    if email and MyUser.objects.filter(email=email).count():
        message = 'El Email ya se encuenta registrado.\n'
    else:
        user = MyUser(
            username=data['email'],
            first_name=data['first_name'],
            last_name=data['last_name'],
            email=data['email'],
            function=data['permission'],
            password=data['password'],
        )
        user.save()
        user.set_password(data['password'])
        user.save()

    return JsonResponse({'success': message})


def edit_user(request, user_id):
    message = 'Ok'
    data = request.POST.get('model')
    data = json.loads(data)
    user = MyUser.objects.get(id=user_id)

    email = data['email']
    if data['email'] != user.email:
        if email and MyUser.objects.filter(email=email).count():
            message = 'Email ya se encuenta registrado.\n'
            return JsonResponse({'status': message})
        user.username = user.email = data['email']

    if data['password'] != 'no_pass':
        user.set_password(data['password'])
    if data['first_name'] != user.first_name:
        user.first_name = data['first_name']
    if data['last_name'] != user.last_name:
        user.last_name = data['last_name']
    if data['permission'] != user.function:
        user.function = data['permission']
    user.save()

    return JsonResponse({'status': message})


def delete_user(request, user_id):
    print "Id a eliminar: ", user_id
    user = MyUser.objects.get(id=user_id)
    user.delete()

    return JsonResponse({'status': 'Ok'})


def get_user_by_id(request, user_id):
    response = {}
    user = MyUser.objects.get(id=user_id)

    response['users'] = []
    response['users'].append({
        'id': user.id,
        'first_name': user.first_name,
        'last_name': user.last_name,
        'email': user.email,
        'permission': user.function,
    })
    return JsonResponse(response)


def get_user_query(request, query):
    response = {}
    response['users'] = []
    users = MyUser.objects.filter(Q(first_name__icontains=query) |
                                  Q(username__icontains=query) |
                                  Q(pk__icontains=query))
    print query
    if users != []:
        response['users'] = create_response_user(users)
        return JsonResponse(response)
    return JsonResponse(response)
