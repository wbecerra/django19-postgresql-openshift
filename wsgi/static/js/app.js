/*jshint multistr: true */
/* AppG8 */


/* Models */
var UsersItem = Backbone.Model.extend({
	defaults: function() {
		return {
			first_name: '',
			last_name: '',
			email: '',
			password: '',
    		permission: '',
		};
	},

	validate: function(attrs){
		message = [];
		expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(attrs.first_name === ''){
			message.push('Es necesario un Nombre de Usuario.');
		}
		if(attrs.email === ''){
			message.push('Es necesario el Email del Usuario');
		}
		if(attrs.password === ''){
			message.push('Es necesario una Contraseña para el Usuario');
		}
		if(! expr.test(attrs.email) && attrs.email != ''){
			message.push('La dirección Email del Usuario es incorrecta.');
		}
		if(attrs.permission === 'no_select'){
			message.push('Debe Elegir una Función para el Usuario.');
		}

		if( message.length == 0 ){
			return '';
		}
		return message;
	},
});


/* Collections */

var UsersCollection = Backbone.Collection.extend({ 
	model: UsersItem,
    url: function () { 
    	if(this.query)
			return '/accounts/all-users';
		if(this.save)
			return '/accounts/adduser/';
		if(this.query_id != '')
			return '/accounts/get_user_by_id/' + this.query_id + '/';
		if(this.query_one != '')
			return '/accounts/get_user_query/' + this.query_one + '/';
		if(this.edit != '')
			return '/accounts/edit_user/' + this.edit + '/';
		if(this.delete != '')
			return '/accounts/delete_user/' + this.delete + '/';
	},

    query: false,
    save: false,
    query_one: '',
    query_id: '',
    edit: '',
    delete: '',

    parse: function(resp) {
		return resp;
	},

});


/* ------ Gobal Vars*/


/* ------ Collection Events */


/* Templates */

var display_all_users = _.template("<tr>\
				<td><%= id %></td>\
				<td><%= first_name %></td>\
				<td><%= last_name %></td>\
				<td><%= email %></td>\
				<td><%= permission %></td>\
				<td style=\"text-align:center;\"><div class=\"btn-group btn-group-xs\" role=\"group\" aria-label=\"actions-<%= id%>\">\
					<button type=\"button\" class=\"btn btn-primary btn-outline\" \
						onClick=\"user_edit_user_modal(<%= id %>)\">\
							<span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"> Editar</span>\
						</button>\
				</div>\
				<div class=\"btn-group btn-group-xs\" role=\"group\" aria-label=\"actions-<%= id%>\">\
					<button type=\"button\" class=\"btn btn-danger btn-outline\" \
						onClick=\"delete_user(<%= id %>)\">\
							<span class=\"glyphicon glyphicon-remove-circle\" aria-hidden=\"true\"> Eliminar</span>\
						</button>\
				</div>\
				</td>\
				</tr>");


/* Functions */

var set_modal_ok = function(title, message, onClick){
	$('#Ok_modal_check').empty();
	$('#OkTitle').html(title);
	if( message === 'Ok' ){
		$('#Ok_modal_check').html("<p style=\"font-size: 128px; text-align: center; color: green;\"><i class=\"fa fa-check\"></i></p>\
	        <p style=\"text-align: center;\">Registro Guardado Correctamente</p>")
	}else{
		$('#Ok_modal_check').append("<p style=\"font-size: 128px; text-align: center; color: red;\"><i class=\"fa fa-close\"></i></p>")
		_.each(message, function(msg){
			$('#Ok_modal_check').append("<p style=\"text-align: center;\">" + msg + "</p>")
		});
	}
	if(onClick != 'false'){
		$('#Ok_button').attr('onClick', onClick);
	}
}


Number.prototype.formatMoney = function(c, d, t){
	var n = this, 
	c = isNaN(c = Math.abs(c)) ? 2 : c, 
	d = d == undefined ? "," : d, 
	t = t == undefined ? "." : t, 
	s = n < 0 ? "-" : "", 
	i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
	j = (j = i.length) > 3 ? j % 3 : 0;
	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};


function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}

/* ---- Users */
var get_all_users = function() {
	var users = new UsersCollection();
	users.query = true;
	users.fetch({success: function (objects, response) {
		$('#user-table-body').empty();
		_.each(response.users, function(user){
			$('#user-table-body').append(display_all_users(user));
		});
	}});
};

var user_add_user_modal_save = function(){
	var userItem = new UsersItem();
	var data = {};
	var title = 'Añadir Usuario';

	data = {
		'first_name': $('#add_user_first_name').val(),
		'last_name': $('#add_user_last_name').val(),
		'email': $('#add_email').val(),
		'password': $('#add_password').val(),
		'permission': $('#add_permission').val(),
	}

	userItem.set(data);
	if(!userItem.isValid()){
		set_modal_ok(title, userItem.validationError, 'false');
		$('#modalOk').modal('show');
	}else{
		var user = new UsersCollection(userItem);
		user.save = true;
		Backbone.emulateJSON = true;
		user.create(data, {
		    wait : true,    // waits for server to respond with 200 before adding newly created model to collection

		    success : function(resp){
		    	var succ = resp.changed.success;
		        if( succ != 'Ok' ){
		        	set_modal_ok(title, resp.changed, 'false');
		        	$('#modalOk').modal('show');
		        }else{
		        	set_modal_ok(title, succ, 'get_all_users()');
					$('#addUserModal').modal('hide');
					$('#modalOk').modal('show');
		        }
	    	}
		});
	}

}

var user_edit_user_modal = function(id){
	var user = new UsersCollection();
	user.query_id = id;
	user.fetch({success: function (objects, response) {
		_.each(response.users, function(user){
			$('#edit_user_first_name').val(user.first_name);
			$('#edit_user_last_name').val(user.last_name);
			$('#edit_email').val(user.email);
			$('#edit_permission').val(user.permission);
			$('#edit_save').attr('onClick', 'user_edit_user_modal_save('+ id +')');
			$('#editUserModal').modal('show');
		});
	}});
}

var user_edit_user_modal_save = function(id){
	var userItem = new UsersItem();
	var data = {};
	var title = 'Editar Usuario';

	password = $('#edit_password').val();
	if(password === ''){
		password = 'no_pass'
	}

	data = {
		'first_name': $('#edit_user_first_name').val(),
		'last_name': $('#edit_user_last_name').val(),
		'email': $('#edit_email').val(),
		'password': password,
		'permission': $('#edit_permission').val(),
	}

	userItem.set(data);
	if(!userItem.isValid()){
		set_modal_ok(title, userItem.validationError, 'false');
		$('#modalOk').modal('show');
	}else{
		var user = new UsersCollection(userItem);
		user.edit = id;
		Backbone.emulateJSON = true;
		user.create(data, {
		    wait : true,    // waits for server to respond with 200 before adding newly created model to collection

		    success : function(resp){
		    	var succ = resp.changed.status;
		        if( succ != 'Ok' ){
		        	set_modal_ok(title, resp.changed, 'false');
		        	$('#modalOk').modal('show');
		        }else{
		        	set_modal_ok(title, succ, 'get_all_users()');
					$('#editUserModal').modal('hide');
					$('#modalOk').modal('show');
		        }
	    	}
		});
	}
}

var delete_user = function(user_id){
	var user = new UsersCollection();
	var title = 'Eliminar Usuario';
	user.delete = user_id;
	user.fetch({success: function (objects, response) {
		set_modal_ok(title, 'Ok', 'get_all_users()');
		$('#modalOk').modal('show');
	}});
}

var user_search_users = function() {
	var users = new UsersCollection()
	users.query_one = $('#searchbox').val();
	users.fetch({success: function (objects, response) {
		if (response.users.length > 0) {
			$('#user-table-body').empty();
			_.each(response.users, function (user) {
				$('#user-table-body').append(display_all_users(user));
			});
		} else {
			$('#user-table-body').empty();
			set_modal_ok('Buscar Usuarios', ['No se encontraron Usuarios'], 'get_all_users()');
			$('#modalOk').modal('show');
		}
	}});
};

/* ---- end Users */